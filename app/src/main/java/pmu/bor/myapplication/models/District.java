package pmu.bor.myapplication.models;

/**
 * Created by Rashid Ahmad on 1/25/2018.
 */

public class District {
    private int Id;
    private String DistrictName;

    public void setDistrictName(String name) {
        this.DistrictName = name;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getDistrictName() {
        return this.DistrictName;
    }

    public int getId() {
        return this.Id;
    }
}
