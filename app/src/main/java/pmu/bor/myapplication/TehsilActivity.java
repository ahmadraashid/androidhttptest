package pmu.bor.myapplication;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class TehsilActivity extends AppCompatActivity {

    String apiUrl = "http://175.107.63.31:9080/api/executeReader/getDistrictTehsilsJson?id=";
    ProgressDialog progressDialog;
    ListView list;
    ArrayList<String> listItems = new ArrayList<String>();
    ArrayList<String> tehsilIds = new ArrayList<String>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tehsil);

        list = findViewById(R.id.listView);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);
        list.setAdapter(adapter);

        String districtId = getIntent().getStringExtra("districtId");
        Log.d("DistrictId: ", districtId);
        apiUrl += districtId;
        TehsilActivity.TehsilAsyncTasks myAsyncTasks = new TehsilAsyncTasks();
        myAsyncTasks.execute();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Selected Tehsil: ", listItems.get(position));
            }
        });
    }

    public class TehsilAsyncTasks extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(TehsilActivity.this);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String current = "";
            try {
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(apiUrl);

                    urlConnection = (HttpURLConnection) url
                            .openConnection();

                    InputStream in = urlConnection.getInputStream();

                    InputStreamReader isw = new InputStreamReader(in);

                    int data = isw.read();
                    while (data != -1) {
                        current += (char) data;
                        data = isw.read();
                        System.out.print(current);
                    }
                    return current;

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
            return current;
        }

        @Override
        protected void onPostExecute(String s) {

            Log.d("data", s.toString());
            progressDialog.dismiss();
            try {
                JSONObject jObject = new JSONObject(s);
                JSONArray jsonArray = jObject.getJSONArray("Result");

                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        // Pulling items from the array
                        String id = obj.getString("Id");
                        String tehsil = obj.getString("TehsilName");
                        adapter.add(tehsil);
                        tehsilIds.add(id);
                    } catch (JSONException e) {
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }
}
