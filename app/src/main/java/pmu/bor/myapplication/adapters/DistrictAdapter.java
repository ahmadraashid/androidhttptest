package pmu.bor.myapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import pmu.bor.myapplication.R;
import pmu.bor.myapplication.models.District;

/**
 * Created by Rashid Ahmad on 1/25/2018.
 */

public class DistrictAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<District> mDataSource;

    public DistrictAdapter(Context context, ArrayList<District> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //1
    @Override
    public int getCount() {
        return mDataSource.size();
    }

    //2
    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    //3
    @Override
    public long getItemId(int position) {
        return position;
    }

    //4
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get view for row item
        View rowView = null;
        //View rowView = mInflater.inflate(R.layout.list_item_recipe, parent, false);

        return rowView;
    }

}


